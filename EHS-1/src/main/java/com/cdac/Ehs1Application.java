package com.cdac;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Ehs1Application {

	public static void main(String[] args) {
		SpringApplication.run(Ehs1Application.class, args);
	}

}
