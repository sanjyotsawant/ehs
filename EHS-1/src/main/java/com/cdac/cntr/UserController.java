package com.cdac.cntr;

import java.lang.ProcessBuilder.Redirect;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.cdac.dao.EHSDao;
import com.cdac.dto.User;

@Controller
@RestController
public class UserController {
	

	@Autowired
	private EHSDao ehsDao;
	
	//@RequestMapping(value = "/signup", method = RequestMethod.GET)
	
	@GetMapping("/signup")
	public ModelAndView signupPage() {
		
		ModelAndView mv = new ModelAndView();
		//anil dada
		mv.setViewName("signup");
		return mv;
				
	}
	

	@PostMapping("/signup")
	public ModelAndView registerUserToDb(User user) {
		
		this.ehsDao.insert(user);
		
		ModelAndView mv = new ModelAndView();
		mv.setViewName("signin");
		return mv;
				
	}

}
